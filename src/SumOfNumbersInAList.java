public class SumOfNumbersInAList {

  public static void main(String[] args) {
    int[] myIntArray = {10,50,20,21};
    int total = 0;
    for (int x : myIntArray) {
      System.out.println(x);
      total = total + x;
    }
    System.out.println("-----\n" + total);
  }
}
